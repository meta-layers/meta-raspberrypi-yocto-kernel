# Enable use of the linux-yocto kernel for the Raspberry Pi 4
KMACHINE:raspberrypi4-64 ?= "bcm-2xxx-rpi4"
COMPATIBLE_MACHINE:raspberrypi4-64 = "(raspberrypi4-64)"
